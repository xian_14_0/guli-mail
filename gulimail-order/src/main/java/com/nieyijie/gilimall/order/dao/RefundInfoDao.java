package com.nieyijie.gilimall.order.dao;

import com.nieyijie.gilimall.order.entity.RefundInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退款信息
 * 
 * @author nieyijie
 * @email 2410156293@qq.com
 * @date 2022-12-27 17:33:51
 */
@Mapper
public interface RefundInfoDao extends BaseMapper<RefundInfoEntity> {
	
}
