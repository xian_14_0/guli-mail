package com.nieyijie.gilimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nieyijie.common.utils.PageUtils;
import com.nieyijie.gilimall.order.entity.OrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author nieyijie
 * @email 2410156293@qq.com
 * @date 2022-12-27 17:33:52
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

