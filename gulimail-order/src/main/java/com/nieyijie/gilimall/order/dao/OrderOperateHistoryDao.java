package com.nieyijie.gilimall.order.dao;

import com.nieyijie.gilimall.order.entity.OrderOperateHistoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单操作历史记录
 * 
 * @author nieyijie
 * @email 2410156293@qq.com
 * @date 2022-12-27 17:33:52
 */
@Mapper
public interface OrderOperateHistoryDao extends BaseMapper<OrderOperateHistoryEntity> {
	
}
