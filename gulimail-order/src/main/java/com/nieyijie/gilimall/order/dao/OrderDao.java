package com.nieyijie.gilimall.order.dao;

import com.nieyijie.gilimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author nieyijie
 * @email 2410156293@qq.com
 * @date 2022-12-27 17:33:52
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
