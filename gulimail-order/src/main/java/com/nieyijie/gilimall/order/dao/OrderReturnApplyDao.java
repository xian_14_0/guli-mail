package com.nieyijie.gilimall.order.dao;

import com.nieyijie.gilimall.order.entity.OrderReturnApplyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单退货申请
 * 
 * @author nieyijie
 * @email 2410156293@qq.com
 * @date 2022-12-27 17:33:52
 */
@Mapper
public interface OrderReturnApplyDao extends BaseMapper<OrderReturnApplyEntity> {
	
}
