package com.nieyijie.gilimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nieyijie.common.to.SkuReductionTo;
import com.nieyijie.common.utils.PageUtils;
import com.nieyijie.gilimall.coupon.entity.SkuFullReductionEntity;

import java.util.Map;

/**
 * 商品满减信息
 *
 * @author nieyijie
 * @email 2410156293@qq.com
 * @date 2022-12-27 17:13:47
 */
public interface SkuFullReductionService extends IService<SkuFullReductionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSkuReduction(SkuReductionTo reductionTo);
}

