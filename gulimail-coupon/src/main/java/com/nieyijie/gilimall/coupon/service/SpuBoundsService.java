package com.nieyijie.gilimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nieyijie.common.utils.PageUtils;
import com.nieyijie.gilimall.coupon.entity.SpuBoundsEntity;

import java.util.Map;

/**
 * 商品spu积分设置
 *
 * @author nieyijie
 * @email 2410156293@qq.com
 * @date 2022-12-27 17:13:47
 */
public interface SpuBoundsService extends IService<SpuBoundsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

