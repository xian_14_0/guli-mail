package com.nieyijie.gilimall.coupon.dao;

import com.nieyijie.gilimall.coupon.entity.SkuFullReductionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品满减信息
 * 
 * @author nieyijie
 * @email 2410156293@qq.com
 * @date 2022-12-27 17:13:47
 */
@Mapper
public interface SkuFullReductionDao extends BaseMapper<SkuFullReductionEntity> {
	
}
