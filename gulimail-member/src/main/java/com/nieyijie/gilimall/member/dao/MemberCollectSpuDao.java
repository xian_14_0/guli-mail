package com.nieyijie.gilimall.member.dao;

import com.nieyijie.gilimall.member.entity.MemberCollectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的商品
 * 
 * @author nieyijie
 * @email 2410156293@qq.com
 * @date 2022-12-27 17:23:57
 */
@Mapper
public interface MemberCollectSpuDao extends BaseMapper<MemberCollectSpuEntity> {
	
}
