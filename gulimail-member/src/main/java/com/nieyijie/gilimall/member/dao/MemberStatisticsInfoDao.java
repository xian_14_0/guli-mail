package com.nieyijie.gilimall.member.dao;

import com.nieyijie.gilimall.member.entity.MemberStatisticsInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员统计信息
 * 
 * @author nieyijie
 * @email 2410156293@qq.com
 * @date 2022-12-27 17:23:57
 */
@Mapper
public interface MemberStatisticsInfoDao extends BaseMapper<MemberStatisticsInfoEntity> {
	
}
