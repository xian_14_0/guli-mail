package com.nieyijie.gilimall.product.feign;

import com.nieyijie.common.to.SkuReductionTo;
import com.nieyijie.common.to.SpuBondTo;
import com.nieyijie.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("gulimail-coupon")
public interface  CouponFeignService {

    @PostMapping("/coupon/spubounds/save")
    R saveSpuBonds(@RequestBody SpuBondTo spuBondTo);
    @PostMapping("/coupon/skufullreduction/saveinfo")
    R saveSkuReduction(@RequestBody SkuReductionTo skuReductionTo);
}
