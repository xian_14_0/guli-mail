package com.nieyijie.gilimall.product.dao;

import com.nieyijie.gilimall.product.entity.AttrGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 属性分组
 * 
 * @author nieyijie
 * @email sunlig2410156293@qq.com
 * @date 2022-12-27 13:45:30
 */
@Mapper
public interface AttrGroupDao extends BaseMapper<AttrGroupEntity> {
	
}
