/**
  * Copyright 2019 bejson.com 
  */
package com.nieyijie.gilimall.product.vo;

import lombok.Data;

@Data
public class Attr {

    private Long attrId;
    private String attrName;
    private String attrValue;

}