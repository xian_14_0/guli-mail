/**
  * Copyright 2019 bejson.com 
  */
package com.nieyijie.gilimall.product.vo;

import lombok.Data;
@Data
public class Images {

    private String imgUrl;
    private int defaultImg;


}