package com.nieyijie.gilimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nieyijie.common.utils.PageUtils;
import com.nieyijie.gilimall.product.entity.BrandEntity;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 品牌
 *
 * @author nieyijie
 * @email sunlig2410156293@qq.com
 * @date 2022-12-27 13:45:30
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

