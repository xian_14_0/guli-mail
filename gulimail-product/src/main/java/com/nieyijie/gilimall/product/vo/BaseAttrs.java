/**
 * Copyright 2019 bejson.com
 */
package com.nieyijie.gilimall.product.vo;

import lombok.Data;

@Data
public class BaseAttrs {

    private Long attrId;
    private String attrValues;
    private int showDesc;



}