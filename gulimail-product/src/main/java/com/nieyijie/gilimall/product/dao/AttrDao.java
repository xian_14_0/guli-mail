package com.nieyijie.gilimall.product.dao;

import com.nieyijie.gilimall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author nieyijie
 * @email sunlig2410156293@qq.com
 * @date 2022-12-27 13:45:30
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
