/**
  * Copyright 2019 bejson.com 
  */
package com.nieyijie.gilimall.product.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Bounds {

    private BigDecimal buyBounds;
    private BigDecimal growBounds;


}