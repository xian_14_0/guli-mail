package com.nieyijie.gilimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nieyijie.common.utils.PageUtils;
import com.nieyijie.gilimall.product.entity.SpuImagesEntity;

import java.util.List;
import java.util.Map;

/**
 * spu图片
 *
 * @author nieyijie
 * @email sunlig2410156293@qq.com
 * @date 2022-12-27 13:45:30
 */
public interface SpuImagesService extends IService<SpuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveImages(Long id, List<String> images);
}

