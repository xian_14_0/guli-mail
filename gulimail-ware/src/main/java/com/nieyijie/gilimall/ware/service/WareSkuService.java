package com.nieyijie.gilimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nieyijie.common.utils.PageUtils;
import com.nieyijie.gilimall.ware.entity.WareSkuEntity;

import java.util.Map;

/**
 * 商品库存
 *
 * @author nieyijie
 * @email 2410156293@qq.com
 * @date 2022-12-27 17:38:24
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

