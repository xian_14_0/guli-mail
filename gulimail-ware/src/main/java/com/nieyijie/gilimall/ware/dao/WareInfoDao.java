package com.nieyijie.gilimall.ware.dao;

import com.nieyijie.gilimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author nieyijie
 * @email 2410156293@qq.com
 * @date 2022-12-27 17:38:24
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
